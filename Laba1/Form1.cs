﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Laba1.GA;

namespace Laba1
{
    public partial class Form1 : Form
    {
        private FitnessFunc fitness;

        private Points points;

        private Population population;

        public Form1()
        {
            InitializeComponent();
            fitness = new FitnessFunc(0, 7, (double t) => { return (t + 1.3) * Math.Sin(0.5 * Math.PI * t + 1); });
            points = new Points(0, 7, 8);
            ShowFitnessFuncChart();
        }

        private void ShowFitnessFuncChart()
        {
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            chart1.Series[2].Points.Clear();

            // Построить график функции
            for (double t = fitness.Min; t < fitness.Max; t += 0.1)
            {
                double Ft = fitness.Call(t);
                chart1.Series[0].Points.AddXY(t, Ft);
            }
        }

        private void ShowPopulationChart(Population population)
        {
            chart1.Series[1].Points.Clear();
            chart1.Series[2].Points.Clear();

            var popList = population.GetOriginalPopulationList();
            foreach (var pointNo in popList)
            {
                double t = points[pointNo];
                double Ft = fitness.Call(t);
                chart1.Series[1].Points.AddXY(t, Ft);
            }

            var bestPointNo = population.GetBestPoint();
            if (bestPointNo > -1)
            {
                double bestFt = fitness.Call(points[bestPointNo]);
                chart1.Series[2].Points.AddXY(points[bestPointNo], bestFt);
            }
        }

        private void ClearPopulationInfo()
        {
            label8.Text = "";
            label9.Text = "";
        }

        private void ShowPopulationInfo(Population population)
        {
            var bestPointNo = population.GetBestPoint();
            if (bestPointNo > -1)
            {
                double bestFt = fitness.Call(points[bestPointNo]);
                label8.Text = String.Format("{0:0.0000}", points[bestPointNo]);
                label9.Text = String.Format("{0:0.0000}", bestFt);
            }
        }

        private void CreateNewPopulation()
        {
            int populationSize;
            int chromosomeLen;
            try
            {
                bool isValid = Int32.TryParse(textBox1.Text, out populationSize) && Int32.TryParse(textBox2.Text, out chromosomeLen);
                if (!Int32.TryParse(textBox1.Text, out populationSize) || populationSize < 1)
                    throw new Exception("Invalid population size");
                if (!Int32.TryParse(textBox2.Text, out chromosomeLen) || chromosomeLen < 1)
                    throw new Exception("Invalid genes count");
                points.BitsCount = chromosomeLen;
                if (populationSize > points.Count)
                    throw new Exception("Max count of population is " + points.Count + " for these function and genes count");

                // Rewrite chart
                ShowFitnessFuncChart();

                population = new Population(populationSize, 0.5, 0.01, points, fitness);
                ShowPopulationChart(population);

                ClearPopulationInfo();

                ShowPopulationInfo(population);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateNewPopulation();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            population.Selection();

            population.Crossingover();

            population.Mutation();

            population.Reduction();

            population.NextGeneration();

            ShowPopulationChart(population);

            ClearPopulationInfo();

            ShowPopulationInfo(population);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ShowFitnessFuncChart();

            ClearPopulationInfo();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                CreateNewPopulation();

                int maxPopulationsCount = 0;
                if (!Int32.TryParse(textBox1.Text, out maxPopulationsCount) || maxPopulationsCount < 1)
                    throw new Exception("Invalid population size");
                for (int i = 1; i < maxPopulationsCount; i++)
                {
                    population.Selection();
                    population.Crossingover();
                    population.Mutation();
                    population.Reduction();
                    population.NextGeneration();
                }

                ShowPopulationChart(population);
                ClearPopulationInfo();
                ShowPopulationInfo(population);

                MessageBox.Show("Вычисления завершены.");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
