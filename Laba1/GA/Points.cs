﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1.GA
{
    class Points
    {
        public Points(double min, double max, int bitsCount)
        {
            Min = min;
            Max = max;
            BitsCount = bitsCount;
        }

        public double Min
        {
            get; set;
        }

        public double Max
        {
            get; set;
        }

        public int BitsCount
        {
            get; set;
        }

        public int Count
        {
            get
            {
                return Convert.ToInt32(Math.Pow(2, BitsCount));
            }
        }

        public double Step
        {
            get
            {
                return (Max - Min) / Count;
            }
        }

        public double this[int index]
        {
            get
            {
                if (index == 0)
                    return Min;
                if (index == Count - 1)
                    return Max;
                if (index > 0 && index < Count - 1)
                    return Min + Step * index;
                throw new IndexOutOfRangeException();
            }
        }

        public string GetBitString(int i)
        {
            string result = Convert.ToString((uint)i, 2);
            if (result.Length < BitsCount)
                result = new string('0', BitsCount - result.Length) + result;
            return result;
        }

        public int GetInt(string bitString)
        {
            return (int)Convert.ToUInt32(bitString, 2);
        }
    }
}
