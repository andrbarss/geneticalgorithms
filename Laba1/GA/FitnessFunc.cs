﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1.GA
{
    delegate double FitnessFuncDelegate(double arg);

    class FitnessFunc
    {
        private FitnessFuncDelegate _fitnessFuncDelegate;

        public FitnessFunc(double min, double max, FitnessFuncDelegate fitnessFuncDelegate)
        {
            Min = min;
            Max = max;
            _fitnessFuncDelegate = fitnessFuncDelegate;
        }

        public double Min
        {
            get; set;
        }

        public double Max
        {
            get; set;
        }

        public double Call(double arg)
        {
            return _fitnessFuncDelegate(arg);
        }
    }
}
