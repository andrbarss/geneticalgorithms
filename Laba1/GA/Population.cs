﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1.GA
{
    class Population
    {
        /// <summary>
        /// Population that present as numbers of points
        /// </summary>
        private IList<int> _originalPopulationList;

        private IList<int> _selectedParentsList;

        private IList<int> _childrenList;

        private IList<int> _newPopulationList;

        private FitnessFunc _func;

        private Points _points;

        public Population(int size, double crossingoverChance, double mutationChance, Points points, FitnessFunc fitnessFunc)
        {
            SIze = size;
            CrossingoverChance = crossingoverChance;
            MutationChance = mutationChance;

            _func = fitnessFunc;
            _points = points;

            _originalPopulationList = new List<int>();
            _selectedParentsList = new List<int>();
            _childrenList = new List<int>();
            _newPopulationList = new List<int>();

            Create();
        }

        public int SIze
        {
            get; private set;
        }

        public double CrossingoverChance
        {
            get; private set;
        }

        public double MutationChance
        {
            get; private set;
        }

        /// <summary>
        /// Create original population
        /// </summary>
        public void Create()
        {
            Clear();

            Random rand = new Random();

            for (int i = 0; i < SIze; i++)
            {
                int pointNo = rand.Next(_points.Count);
                _originalPopulationList.Add(pointNo);
            }

            /*int rangeSize = Convert.ToInt32(Math.Truncate(Convert.ToDecimal(_points.Count / SIze)));
            int rest = Convert.ToInt32(_points.Count % SIze);
            int rangesCount = Convert.ToInt32((_points.Count - rest) / rangeSize);
            if (rest > 0)
                rangesCount--;

            for (int rangeInd = 0; rangeInd < rangesCount; rangeInd++)
            {
                int rangeStart = rangeInd * rangeSize;
                int rangeEnd = rangeStart + rangeSize - 1;
                int pointNo = rand.Next(rangeStart, rangeEnd);
                _originalPopulationList.Add(pointNo);
            }
            
            if (rest == 1)
                _originalPopulationList.Add(_points.Count - 1);

            if (rest > 1)
            {
                int rangeEnd = _points.Count - 1;
                int rangeStart = rangeEnd - rest + 1;
                int pointNo = rand.Next(rangeStart, rangeEnd);
                _originalPopulationList.Add(pointNo);
            }*/
        }

        /// <summary>
        /// Selection of parents for reproduction of new population. Using tournier strategy.
        /// </summary>
        public void Selection()
        {
            _selectedParentsList.Clear();

            Random random1 = new Random();
            Random random2 = new Random();

            for (int i = 0; i < SIze; i++)
            {
                // Random selection of first challenger from original population
                int i1 = random1.Next(SIze - 1);
                int parent1 = _originalPopulationList[i1];

                // Random selection of second challenger from original population
                int i2 = random2.Next(SIze - 1);
                int parent2 = _originalPopulationList[i2];

                // Add to selection more fittable challenger (that have higher value of fitness function)
                if (_func.Call(_points[parent1]) > _func.Call(_points[parent2]))
                    _selectedParentsList.Add(parent1);
                else
                    _selectedParentsList.Add(parent2);
            }
        }

        public void Crossingover()
        {
            _childrenList.Clear();

            Random rand1 = new Random();
            Random rand2 = new Random();

            for (int i = 0; i < SIze - 1; i++)
            {
                int parent1 = _selectedParentsList[i];
                for (int j = i + 1; j < SIze; j++)
                {
                    int parent2 = _selectedParentsList[j];
                    double p = rand1.NextDouble();
                    if (parent1 == parent2 || p > CrossingoverChance)
                        continue;

                    string chromosome1 = _points.GetBitString(parent1);
                    string chromosome2 = _points.GetBitString(parent2);
                    int k = rand2.Next(_points.BitsCount - 1); // Search point of exchange

                    Exchange(ref chromosome1, ref chromosome2, k);

                    int child1 = _points.GetInt(chromosome1);
                    int child2 = _points.GetInt(chromosome2);

                    _childrenList.Add(child1);
                    _childrenList.Add(child2);
                }
            }
        }

        public void Mutation()
        {
            Random rand1 = new Random();
            Random rand2 = new Random();

            for (int i = 0; i < _childrenList.Count; i++)
            {
                double p = rand1.NextDouble();
                if (p <= MutationChance)
                {
                    // Change position of mutation
                    int pos = rand2.Next(0, _points.BitsCount - 1);

                    // Get chromosome
                    string chromosome = _points.GetBitString(_childrenList[i]);

                    // Change gene in chromosome
                    var strBuilder = new StringBuilder(chromosome);
                    strBuilder[pos] = (chromosome[pos] == '1') ? '0' : '1';
                    chromosome = strBuilder.ToString();

                    // Save value
                    _childrenList[i] = _points.GetInt(chromosome);
                }
            }
        }

        public void Reduction()
        {
            _newPopulationList.Clear();

            for (int i = 0; i < _originalPopulationList.Count; i++)
            {
                if (!_newPopulationList.Contains(_originalPopulationList[i]))
                    _newPopulationList.Add(_originalPopulationList[i]);
            }

            for (int i = 0; i < _childrenList.Count; i++)
            {
                if (!_newPopulationList.Contains(_childrenList[i]))
                    _newPopulationList.Add(_childrenList[i]);
            }

            if (_newPopulationList.Count > SIze)
            {
                var orderedPopulation = _newPopulationList.OrderByDescending(x => _func.Call(_points[x])).ToArray();
                var destArray = new int[SIze];
                Array.Copy(orderedPopulation, destArray, SIze);
                _newPopulationList = destArray.ToList();
            }
            else
            {
                Random rand = new Random();
                while (SIze - _newPopulationList.Count > 0)
                {
                    int pointNo = rand.Next(_points.Count);
                    if (!_newPopulationList.Contains(pointNo))
                        _newPopulationList.Add(pointNo);
                }
            }
        }

        public void NextGeneration()
        {
            SIze = _newPopulationList.Count;

            _originalPopulationList = _newPopulationList.ToList();
        }

        public static void Exchange(ref string s1, ref string s2, int k)
        {
            string tmp1 = s1.Substring(k);
            string tmp2 = s2.Substring(k);

            s1 = s1.Substring(0, k) + tmp2;
            s2 = s2.Substring(0, k) + tmp1;
        }

        /// <summary>
        /// Get list of points that is original population
        /// </summary>
        /// <returns></returns>
        public IList<int> GetOriginalPopulationList()
        {
            return _originalPopulationList;
        }

        public void Clear()
        {
            _originalPopulationList.Clear();
        }

        public int GetBestPoint()
        {
            var popList = GetOriginalPopulationList();
            if (popList.Count > 0)
            {
                int bestPointNo = popList[0];
                double bestFt = _func.Call(_points[bestPointNo]);
                for (int pointNo = 0; pointNo < popList.Count; pointNo++)
                {
                    double Ft = _func.Call(_points[pointNo]);
                    if (Ft >= bestFt)
                    {
                        bestFt = Ft;
                        bestPointNo = pointNo;
                    }
                }
                return bestPointNo;
            }
            return -1;
        }
    }
}
