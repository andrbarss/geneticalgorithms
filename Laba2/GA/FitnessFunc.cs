﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2.GA
{
    delegate double FitnessFuncDelegate(double x1, double x2);

    class FitnessFunc
    {
        private FitnessFuncDelegate _fitnessFunc;

        public FitnessFunc(double min, double max, FitnessFuncDelegate func)
        {
            Min = min;
            Max = max;
            _fitnessFunc = func;
        }

        public double Min
        {
            get; set;
        }

        public double Max
        {
            get; set;
        }

        public double Call(double x1, double x2)
        {
            if (x1 < Min || x1 > Max)
            {
                throw new ArgumentOutOfRangeException("x1", x1, String.Format("Value must be in range [{0:0.00000}, {1:0.00000}]", Min, Max));
            }
            if (x2 < Min || x2 > Max)
            {
                throw new ArgumentOutOfRangeException("x2", x2, String.Format("Value must be in range [{0:0.00000}, {1:0.00000}]", Min, Max));
            }

            return _fitnessFunc(x1, x2);
        }
    }
}
