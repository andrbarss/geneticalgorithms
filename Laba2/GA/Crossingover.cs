﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba2.Points;
using Laba2.Helpers;

namespace Laba2.GA
{
    class Crossingover
    {
        private IList<Point> _items;

        private IList<Tuple<int, int>> _pairs;

        private double _crossingoverProbability;

        public Crossingover(IList<Point> items, double crossingoverProbability)
        {
            _items = items;
            _crossingoverProbability = crossingoverProbability;
        }

        public void CreatePairs()
        {
            _pairs = new List<Tuple<int, int>>();

            var usedItems = new bool[_items.Count];

            while (_pairs.Count < _items.Count / 2)
            {
                var val1 = RandHelper.GenerateInt32(0, _items.Count);
                while (usedItems[val1])
                {
                    val1 = RandHelper.GenerateInt32(0, _items.Count);
                }
                usedItems[val1] = true;

                var val2 = RandHelper.GenerateInt32(0, _items.Count);
                while (usedItems[val2])
                {
                    val2 = RandHelper.GenerateInt32(0, _items.Count);
                }
                usedItems[val2] = true;

                _pairs.Add(new Tuple<int, int>(val1, val2));
            }
        }

        private Tuple<Point, Point> CrossingoverPair(Tuple<int, int> pair)
        {
            var point1 = _items[pair.Item1];
            var point2 = _items[pair.Item2];

            var crossIndex1 = RandHelper.GenerateDouble();
            var crossIndex2 = RandHelper.GenerateDouble();

            var crossedFirstRealItem = new Point
            {
                X1 = GetCrossedValue(point1.X1, point2.X1, crossIndex1),
                X2 = GetCrossedValue(point1.X2, point2.X2, crossIndex2)
            };
            var crossedSecondRealItem = new Point
            {
                X1 = GetCrossedValue(point2.X1, point1.X1, crossIndex1),
                X2 = GetCrossedValue(point2.X2, point1.X2, crossIndex2)
            };

            return new Tuple<Point, Point>(crossedFirstRealItem, crossedSecondRealItem);
        }

        private static double GetCrossedValue(double x1, double x2, double alpha)
        {
            return x1 + alpha * (x2 - x1);
        }

        public IList<Point> Perform()
        {
            CreatePairs();

            var newItems = new List<Point>();
            foreach (var pair in _pairs)
            {
                var crossingoverItems = CrossingoverPair(pair);

                newItems.Add(crossingoverItems.Item1);
                newItems.Add(crossingoverItems.Item2);
            }
            return newItems;
        }
    }
}
