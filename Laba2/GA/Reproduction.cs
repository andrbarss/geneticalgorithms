﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba2.Points;
using Laba2.Helpers;

namespace Laba2.GA
{
    class Reproduction
    {
        private IList<Point> _population;

        private FitnessFunc _func;

        private IDictionary<Point, ItemInfo> _itemsResultsDictionary;

        public Reproduction(IList<Point> population, FitnessFunc func)
        {
            _population = population;
            _func = func;
            Prepare();
        }

        protected void Prepare()
        {
            _itemsResultsDictionary = _population.ToDictionary(p => p, p => new ItemInfo { FunctionValue = _func.Call(p.X1, p.X2) });

            var maxFuncValue = _itemsResultsDictionary.Max(p => p.Value.FunctionValue);
            foreach (var item in _itemsResultsDictionary.Values)
            {
                item.FunctionValue = maxFuncValue - item.FunctionValue;
            }

            var resultsSum = _itemsResultsDictionary.Sum(pair => Math.Abs(pair.Value.FunctionValue));

            foreach (var itemInfo in _itemsResultsDictionary)
            {
                itemInfo.Value.NormalizedValue = itemInfo.Value.FunctionValue / resultsSum;
                itemInfo.Value.ExpectedNumberOfCopies = itemInfo.Value.NormalizedValue * _population.Count;
                itemInfo.Value.RealNumberOfCopies = Convert.ToInt32(Math.Round(itemInfo.Value.ExpectedNumberOfCopies));
            }
        }

        public IList<Point> Roulette()
        {
            // Create list of candidates for crossingover
            var items = new List<Point>();
            while (items.Count < _itemsResultsDictionary.Count)
            {
                // Randomized selection of parents
                var randChoose = RandHelper.GenerateDouble();
                double itemsResultsSum = 0;
                foreach (var item in _itemsResultsDictionary)
                {
                    itemsResultsSum += item.Value.NormalizedValue;
                    if (randChoose <= itemsResultsSum)
                    {
                        items.Add(item.Key);
                        break;
                    }
                }
            }
            return items;
        }
    }
}
