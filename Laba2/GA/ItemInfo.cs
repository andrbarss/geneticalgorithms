﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2.GA
{
    class ItemInfo
    {
        public double FunctionValue { get; set; }

        public double NormalizedValue { get; set; }

        public double ExpectedNumberOfCopies { get; set; }

        public int RealNumberOfCopies { get; set; }

        public int CurrentNumberOfCopies { get; set; } = 0;
    }
}
