﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba2.GA;
using Laba2.Points;
using Laba2.Helpers;

namespace Laba2
{
    class GenAlgo
    {
        public GenAlgo()
        {
            FitnessFuncDelegate _del = (double x1, double x2) => (Math.Pow(Math.Pow(x1, 2) + Math.Pow(x2, 2), 0.25) * (Math.Pow(Math.Sin(50 * Math.Pow(Math.Pow(x1, 2) + Math.Pow(x2, 2), 0.1)), 2) + 1));
            //FitnessFuncDelegate _del = (double x1, double x2) => (100 * Math.Pow(Math.Pow(x1, 2) + x2, 2) + Math.Pow(1 - x1, 2));
            Function = new FitnessFunc(-100, 100, _del);
            CurrentPopulation = new List<Point>();
        }

        public IList<Point> CreatePopulation(int populationSize)
        {
            // Create new population
            CurrentPopulation = new List<Point>();

            for (var i = 0; i < populationSize; i++)
            {
                var x1 = RandHelper.GenerateNumber(Convert.ToInt32(Function.Min), Convert.ToInt32(Function.Max), Accuracy);
                var x2 = RandHelper.GenerateNumber(Convert.ToInt32(Function.Min), Convert.ToInt32(Function.Max), Accuracy);
                CurrentPopulation.Add(new Point(x1, x2));
            }

            return CurrentPopulation;
        }

        public IList<Point> NextPopulation()
        {
            // Select
            // Crossingover
            // Mutation
            // Reduction

            var reprod = new Reproduction(CurrentPopulation, Function);
            var reproduceItems = reprod.Roulette();

            var crossing = new Crossingover(reproduceItems, CrossingoverProbability);
            var newItems = crossing.Perform();

            CurrentPopulation = newItems;

            return CurrentPopulation;
        }

        public FitnessFunc Function
        {
            get; private set;
        }

        public IList<Point> CurrentPopulation { get; private set; }

        public int Accuracy { get; set; } = 3;

        public double CrossingoverProbability { get; set; } = 1;
    }
}
