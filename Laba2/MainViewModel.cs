﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Laba2.GA;
using OxyPlot;
using OxyPlot.Series;
using System.Threading;
using System.Windows;

namespace Laba2
{
    class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            Algo = new GenAlgo();
            ChartModel = new PlotModel();

            //generate values
            Func<double, double, double> peaks = (x1, x2) => Algo.Function.Call(x1, x2);
            var xx = ArrayBuilder.CreateVector(Algo.Function.Min, Algo.Function.Max, 10);
            var yy = ArrayBuilder.CreateVector(Algo.Function.Min, Algo.Function.Max, 10);
            var peaksData = ArrayBuilder.Evaluate(peaks, xx, yy);

            var cs = new ContourSeries
            {
                Color = OxyColors.Black,
                LabelBackground = OxyColors.White,
                ColumnCoordinates = yy,
                RowCoordinates = xx,
                Data = peaksData
            };
            ChartModel.Series.Add(cs);

            var solutions = new ScatterSeries
            {
                MarkerType = MarkerType.Circle,
                MarkerFill = OxyColor.FromRgb(0xFF, 0, 0)
            };
            ChartModel.Series.Add(solutions);

            ChartModel.Series.Add(new ScatterSeries());

            PopulationSize = "300";
            Accuracy = "3";
            CrosoverProbability = "0.8";
            MutationProbability = "0.05";
            MaxPopulations = "500";
        }

        public PlotModel ChartModel { get; private set; }

        public GenAlgo Algo { get; private set; }
        
        private string _populationSize;
        public String PopulationSize
        {
            get
            {
                return _populationSize;
            }
            set
            {
                _populationSize = value;
                NotifyPropertyChanged();
            }
        }
        
        private string _accuracy;
        public String Accuracy
        {
            get
            {
                return _accuracy;
            }
            set
            {
                _accuracy = value;
                NotifyPropertyChanged();
            }
        }
        
        private string _crosoverProbability;
        public String CrosoverProbability
        {
            get
            {
                return _crosoverProbability;
            }
            set
            {
                _crosoverProbability = value;
                NotifyPropertyChanged();
            }
        }

        private string _mutationProbability;
        public String MutationProbability
        {
            get
            {
                return _mutationProbability;
            }
            set
            {
                _mutationProbability = value;
                NotifyPropertyChanged();
            }
        }

        private string _maxPopulations;
        public String MaxPopulations
        {
            get
            {
                return _maxPopulations;
            }
            set
            {
                _maxPopulations = value;
                NotifyPropertyChanged();
            }
        }

        private string _bestX1;
        public String BestX1
        {
            get
            {
                return _bestX1;
            }
            set
            {
                _bestX1 = value;
                NotifyPropertyChanged();
            }
        }

        private string _bestX2;
        public String BestX2
        {
            get
            {
                return _bestX2;
            }
            set
            {
                _bestX2 = value;
                NotifyPropertyChanged();
            }
        }

        private string _bestZ;
        public String BestZ
        {
            get
            {
                return _bestZ;
            }
            set
            {
                _bestZ = value;
                NotifyPropertyChanged();
            }
        }

        public event EventHandler OnComplete;

        private int _maxPopulationsCount = 0;

        public int MaxPopulationsCount { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        Thread _thread;

        protected virtual void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void CreateNewPopulation()
        {
            int populationSize;
            if (!Int32.TryParse(PopulationSize, out populationSize))
            {
                throw new Exception("Population size must be a number");
            }
            
            if (!Int32.TryParse(MaxPopulations, out _maxPopulationsCount))
            {
                _maxPopulationsCount = 0;
                throw new Exception("Max populations count must be a number");
            }

            int accuracy;
            if (!Int32.TryParse(Accuracy, out accuracy))
            {
                throw new Exception("Accuracy must be a number");
            }

            Algo.Accuracy = accuracy;

            Algo.CreatePopulation(populationSize);

            _maxPopulationsCount--;

            DrawPoints(Algo.CurrentPopulation);

            ShowInfo(Algo.CurrentPopulation);
        }

        public void NextPopulation()
        {
            if (_maxPopulationsCount > 0 && !HasSolution())
            {
                Algo.NextPopulation();

                _maxPopulationsCount--;

                DrawPoints(Algo.CurrentPopulation);

                ShowInfo(Algo.CurrentPopulation);
            }
        }

        protected void SearchSolution()
        {
            while (_maxPopulationsCount > 0 && !HasSolution())
            {
                Algo.NextPopulation();

                _maxPopulationsCount--;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    DrawPoints(Algo.CurrentPopulation);

                    ShowInfo(Algo.CurrentPopulation);
                });

                Thread.Sleep(100);
            }

            OnComplete?.Invoke(this, EventArgs.Empty);
        }

        public void Start()
        {
            _thread = new Thread(SearchSolution);
            _thread.Start();
        }

        public bool HasSolution()
        {
            if (_maxPopulationsCount == 0)
                return true;

            // Check if all items in one point
            var population = Algo.CurrentPopulation;
            var itemsResultsDictionary = population.ToDictionary(p => p, p => new ItemInfo { FunctionValue = Algo.Function.Call(p.X1, p.X2) });

            var maxFuncValue = itemsResultsDictionary.Max(p => p.Value.FunctionValue);
            foreach (var item in itemsResultsDictionary.Values)
            {
                item.FunctionValue = maxFuncValue - item.FunctionValue;
            }

            var resultsSum = itemsResultsDictionary.Sum(pair => Math.Abs(pair.Value.FunctionValue));
            return resultsSum == 0;
        }

        protected void DrawPoints(IList<Points.Point> points)
        {
            // Clear old points
            ClearPlot();

            //var itemsResultsDict = points.ToDictionary(p => p, p => Algo.Function.Call(p.X1, p.X2));

            var solutions = new ScatterSeries
            {
                MarkerType = MarkerType.Circle,
                MarkerFill = OxyColor.FromRgb(0, 0, 0xFF)
            };
            foreach (var p in points)
            {
                var plotPoint = new ScatterPoint(p.X1, p.X2, 5);
                solutions.Points.Add(plotPoint);
            }
            ChartModel.Series.Add(solutions);

            var minPoint = GetMinPoint(points);
            if (minPoint != null)
            {
                var minValueSeries = new ScatterSeries
                {
                    MarkerType = MarkerType.Circle,
                    MarkerFill = OxyColor.FromRgb(0xFF, 0, 0)
                };
                minValueSeries.Points.Add(new ScatterPoint(minPoint.Item1.X1, minPoint.Item1.X2, 7, minPoint.Item2));
                ChartModel.Series.Add(minValueSeries);
            }

            // Refresh plot
            ChartModel.InvalidatePlot(true);
        }

        protected Tuple<Points.Point, double> GetMinPoint(IList<Points.Point> points)
        {
            if (points.Count > 0)
            {
                var itemsResultsDict = points.ToDictionary(p => p, p => Algo.Function.Call(p.X1, p.X2));
                var minPoint = itemsResultsDict.First().Key;
                var minF = itemsResultsDict[minPoint];
                for (int i = 1; i < itemsResultsDict.Count; i++)
                {
                    var point = itemsResultsDict.Keys.ElementAt(i);
                    var f = itemsResultsDict[point];
                    if (f <= minF)
                    {
                        minF = f;
                        minPoint = point;
                    }
                }
                return new Tuple<Points.Point, double>(minPoint, minF);
            }
            return null;
        }

        protected void ShowInfo(IList<Points.Point> points)
        {
            var minPoint = GetMinPoint(points);
            if (minPoint != null)
            {
                BestX1 = String.Format("{0:0.00000}", minPoint.Item1.X1);
                BestX2 = String.Format("{0:0.00000}", minPoint.Item1.X2);
                BestZ = String.Format("{0:0.00000}", minPoint.Item2);
            }
        }

        public void ClearPlot()
        {
            for (int i = ChartModel.Series.Count - 1; i > 0; i--)
            {
                ChartModel.Series.RemoveAt(i);
            }
        }

        public void Clear()
        {
            if (_thread != null)
            {
                try
                {
                    _thread.Abort();
                }
                catch { }
            }
            _maxPopulationsCount = 0;
            ClearPlot();
            ChartModel.InvalidatePlot(true);
            BestX1 = "";
            BestX2 = "";
            BestZ = "";
        }
    }
}
