﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2.Helpers
{
    class RandHelper
    {
        private static readonly Random RandomGenerator;

        static RandHelper()
        {
            RandomGenerator = new Random(DateTime.Now.Second + DateTime.Now.Millisecond);
        }

        public static double GenerateNumber(int min, int max, int accuracy = 3)
        {
            var intValue = RandomGenerator.Next(min + 1, max - 1);
            var afterDotValue = RandomGenerator.Next(Convert.ToInt32(Math.Pow(10, accuracy) - 1));

            return Convert.ToDouble($"{intValue}.{afterDotValue}");
        }

        public static double GenerateDouble()
        {
            return RandomGenerator.NextDouble();
        }

        public static int GenerateInt32(int min, int max)
        {
            return RandomGenerator.Next(min, max);
        }
    }
}
