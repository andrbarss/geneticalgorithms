﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3.Helpers
{
    class RandHelper
    {
        private static readonly Random _randomGenerator;

        public static Random RandomGenerator
        {
            get => _randomGenerator;
        }

        static RandHelper()
        {
            _randomGenerator = new Random(DateTime.Now.Second + DateTime.Now.Millisecond);
        }

        public static int[] GenerateRandArray(int count)
        {
            var resultArray = new int[count];
            var sequence = Enumerable.Range(0, count).ToList();

            int index = 0;
            while (sequence.Count > 0)
            {
                var randElementIndex = RandomGenerator.Next(0, sequence.Count);
                resultArray[index] = sequence[randElementIndex];
                index++;
                sequence.RemoveAt(randElementIndex);
            }

            return resultArray;
        }
    }
}
