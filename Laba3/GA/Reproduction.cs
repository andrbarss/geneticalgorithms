﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba3.Helpers;

namespace Laba3.GA
{
    public class Reproduction
    {
        private IList<int[]> _population;

        private IList<Tuple<double, double>> _items;

        private int _populationSize;

        private FitnessFunc _func;

        private IDictionary<int[], ItemInfo> _itemsResultsDictionary;

        public Reproduction(IList<int[]> population, IList<Tuple<double, double>> items, int populationSize, FitnessFunc func)
        {
            _population = population;
            _items = items;
            _populationSize = populationSize;
            _func = func;
            Prepare();
        }

        private void Prepare()
        {
            _itemsResultsDictionary = _population.ToDictionary(p => p, p => new ItemInfo { FunctionValue = _func.Call(p, _items) });

            var maxFuncValue = _itemsResultsDictionary.Max(p => p.Value.FunctionValue);
            foreach (var item in _itemsResultsDictionary.Values)
            {
                item.FunctionValue = maxFuncValue - item.FunctionValue;
            }

            var resultsSum = _itemsResultsDictionary.Sum(pair => Math.Abs(pair.Value.FunctionValue));

            foreach (var itemInfo in _itemsResultsDictionary)
            {
                itemInfo.Value.NormalizedValue = itemInfo.Value.FunctionValue / resultsSum;
                itemInfo.Value.ExpectedNumberOfCopies = itemInfo.Value.NormalizedValue * _population.Count;
                itemInfo.Value.RealNumberOfCopies = Convert.ToInt32(Math.Round(itemInfo.Value.ExpectedNumberOfCopies));
            }
        }

        public IList<int[]> Roulette()
        {
            var result = new List<int[]>();

            while (result.Count < _populationSize)
            {
                var randChoose = RandHelper.RandomGenerator.NextDouble();
                double sum = 0;
                foreach (var itemInfo in _itemsResultsDictionary)
                {
                    sum += itemInfo.Value.NormalizedValue;
                    if (randChoose > sum ||
                        (itemInfo.Value.RealNumberOfCopies == itemInfo.Value.CurrentNumberOfCopies &&
                        itemInfo.Value.RealNumberOfCopies != 0))
                    {
                        continue;
                    }

                    result.Add(itemInfo.Key);
                    itemInfo.Value.CurrentNumberOfCopies++;
                    break;
                }
            }

            return result;
        }

        public IList<int[]> Selection()
        {
            var result = new List<int[]>();

            while (result.Count < _populationSize)
            {
                // Random selection of first challenger from original population
                int i1 = RandHelper.RandomGenerator.Next(0, _populationSize);
                var parent1 = _population[i1];
                var funcValue1 = _itemsResultsDictionary[parent1].FunctionValue;

                // Random selection of second challenger from original population
                int i2 = RandHelper.RandomGenerator.Next(0, _populationSize);
                var parent2 = _population[i2];
                var funcValue2 = _itemsResultsDictionary[parent2].FunctionValue;

                // Add to selection more fittable challenger (that have lower value of fitness function)
                if (funcValue1 > funcValue2)
                    result.Add(parent2);
                else
                    result.Add(parent1);
            }

            return result;
        }
    }
}
