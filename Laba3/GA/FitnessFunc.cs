﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3.GA
{
    public class FitnessFunc
    {
        public FitnessFunc() { }

        public double Call(int[] solution, IList<Tuple<double, double>> items)
        {
            double result = 0;

            for (var i = 1; i < solution.Length; i++)
            {
                result += Math.Sqrt(Math.Pow(items[solution[i]].Item1 - items[solution[i - 1]].Item1, 2) + 
                    Math.Pow(items[solution[i]].Item2 - items[solution[i - 1]].Item2, 2));
            }

            return result;
        }
    }
}
