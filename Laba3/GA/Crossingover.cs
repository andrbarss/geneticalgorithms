﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba3.Helpers;

namespace Laba3.GA
{
    class Crossingover
    {
        private IList<int[]> _items;

        private IList<Tuple<int, int>> _pairs;

        public Crossingover(IList<int[]> reproducedItems)
        {
            _items = reproducedItems;
        }

        public IList<int[]> Perform()
        {
            var nextGeneration = new List<int[]>();

            CreatePairs();

            foreach (var pair in _pairs)
            {
                var crossingoverItems = CrossingoverPair(pair);

                nextGeneration.Add(crossingoverItems.Item1);
                nextGeneration.Add(crossingoverItems.Item2);
            }

            return nextGeneration;
        }

        public void CreatePairs()
        {
            _pairs = new List<Tuple<int, int>>();

            var usedItems = new bool[_items.Count];

            while (_pairs.Count < _items.Count / 2)
            {
                var val1 = RandHelper.RandomGenerator.Next(0, _items.Count);
                while (usedItems[val1])
                {
                    val1 = RandHelper.RandomGenerator.Next(0, _items.Count);
                }
                usedItems[val1] = true;

                var val2 = RandHelper.RandomGenerator.Next(0, _items.Count);
                while (usedItems[val2])
                {
                    val2 = RandHelper.RandomGenerator.Next(0, _items.Count);
                }
                usedItems[val2] = true;

                _pairs.Add(new Tuple<int, int>(val1, val2));
            }
        }

        private Tuple<int[], int[]> CrossingoverPair(Tuple<int, int> pair)
        {
            var first = _items[pair.Item1];
            var second = _items[pair.Item2];

            var genesCount = first.Length;

            // Get split point
            var splitIndex1 = RandHelper.RandomGenerator.Next(0, genesCount - 1);
            var splitIndex2 = RandHelper.RandomGenerator.Next(splitIndex1 + 1, genesCount - 1);

            // Prepare children
            var firstChidren = new int[genesCount];
            var secondChildren = new int[genesCount];
            for (var i = 0; i < genesCount; i++)
            {
                firstChidren[i] = -1;
                secondChildren[i] = -1;
            }

            // Exchange parts
            for (var index = splitIndex1 + 1; index <= splitIndex2; index++)
            {
                firstChidren[index] = second[index];
                secondChildren[index] = first[index];
            }

            // Replace -1 to values from parents for first part of gene
            for (var index = 0; index <= splitIndex1; index++)
            {
                SetFromParent(first, firstChidren, index);
                SetFromParent(second, secondChildren, index);
            }

            // Replace -1 to values from parents for second part of gene
            for (var index = splitIndex2 + 1; index < genesCount; index++)
            {
                SetFromParent(first, firstChidren, index);
                SetFromParent(second, secondChildren, index);
            }

            for (var index = 0; index < genesCount; index++)
            {
                SetFromAnotherParent(firstChidren, second, index);
                SetFromAnotherParent(secondChildren, first, index);
            }

            return new Tuple<int[], int[]>(firstChidren, secondChildren);
        }

        private static void SetFromAnotherParent(int[] crossedSolution, int[] parent, int index)
        {
            if (crossedSolution[index] != -1)
            {
                return;
            }

            crossedSolution[index] = parent[index];
        }

        private static void SetFromParent(int[] parent, int[] crossedSolution, int index)
        {
            var valueFromParent = parent[index];

            if (crossedSolution.All(value => value != valueFromParent))
            {
                crossedSolution[index] = valueFromParent;
            }
        }
    }
}
