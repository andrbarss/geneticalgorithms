﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Laba3.Helpers;
using Laba3.GA;

namespace Laba3
{
    public class GenAlgo
    {
        public GenAlgo()
        {
            Items = new List<Tuple<double, double>>();
            CurrentPopulation = new List<int[]>();
            Function = new FitnessFunc();
        }

        public IList<int[]> CreatePopulation(int populationSize)
        {
            if (Items.Count == 0)
            {
                throw new Exception("Cities is not loaded");
            }

            PopulationSize = populationSize;
            CurrentPopulation = new List<int[]>();
            for (var i = 0; i < PopulationSize; i++)
            {
                CurrentPopulation.Add(RandHelper.GenerateRandArray(Items.Count));
            }
            return CurrentPopulation;
        }

        public IList<int[]> Reproduce()
        {
            if (CurrentPopulation == null || CurrentPopulation.Count == 0)
            {
                throw new Exception("Population wasn't created");
            }
            Reproduction reprod = new Reproduction(CurrentPopulation, Items, PopulationSize, Function);
            return reprod.Roulette();
        }

        public IList<int[]> Cross(IList<int[]> selItems)
        {
            Crossingover crossingover = new Crossingover(selItems);
            return crossingover.Perform();
        }

        public IList<int[]> Reduction(IList<int[]> newPopulation)
        {
            var commonPopulation = new List<int[]>();

            foreach (var item in newPopulation)
            {
                commonPopulation.Add(item);
            }

            foreach (var item in CurrentPopulation)
            {
                if (!commonPopulation.Contains(item))
                {
                    commonPopulation.Add(item);
                }
            }

            var resultsValues = commonPopulation.OrderBy(item => Function.Call(item, Items)).ToList();
            
            if (resultsValues.Count() > PopulationSize)
            {
                return resultsValues.GetRange(0, PopulationSize);
            }
            return resultsValues;
        }

        public IList<int[]> NextPopulation()
        {
            var selectedItems = Reproduce();
            var newPopulation = Cross(selectedItems);
            var reduced = Reduction(newPopulation);

            CurrentPopulation = reduced;

            return CurrentPopulation;
        }

        public Tuple<int[], double> GetBestSolution()
        {
            if (CurrentPopulation.Count > 0)
            {
                var bestSolution = CurrentPopulation[0];
                var bestFitnessValue = Function.Call(bestSolution, Items);
                for (int i = 1; i < CurrentPopulation.Count; i++)
                {
                    var solution = CurrentPopulation[i];
                    var fitnessValue = Function.Call(bestSolution, Items);
                    if (bestFitnessValue > fitnessValue)
                    {
                        bestFitnessValue = fitnessValue;
                        bestSolution = solution;
                    }
                }
                return new Tuple<int[], double>(bestSolution, bestFitnessValue);
            }
            return null;
        }

        public IList<Tuple<double, double>> Items { get; set; }

        public IList<int[]> CurrentPopulation { get; private set; }

        public int PopulationSize { get; private set; }

        public FitnessFunc Function { get; private set; }
    }
}
