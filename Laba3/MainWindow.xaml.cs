﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Laba3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainViewModel ViewModel
        {
            get => DataContext as MainViewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.OpenFile();
            ViewModel.ShowInfo();
            ViewModel.DrawChart();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            /*do
            {*/
            if (!ViewModel.HasSolution())
            {
                ViewModel.NextStep();
                ViewModel.ShowInfo();
                ViewModel.DrawChart();
                if (ViewModel.HasSolution())
                {
                    MessageBox.Show("Done!");
                }
            }
            /*}
            while (!ViewModel.HasSolution());

            MessageBox.Show("Done!");*/
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ViewModel.Clear();
        }
    }
}
