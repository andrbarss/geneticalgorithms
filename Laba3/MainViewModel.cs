﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Microsoft.Win32;
using System.IO;

namespace Laba3
{
    public class MainViewModel: INotifyPropertyChanged
    {
        public MainViewModel()
        {
            ChartModel = new PlotModel();
            PopulationSize = "10";
            GenerationMaxCount = "100";

            BestRoute = new List<string>();
            Algo = new GenAlgo();
        }

        public void OpenFile()
        {
            int _populationSize;
            if (!Int32.TryParse(PopulationSize, out _populationSize))
            {
                throw new Exception("Population size must be a number");
            }

            int _generationsMaxCount;
            if (!Int32.TryParse(GenerationMaxCount, out _generationsMaxCount) && _leftGenerations <= 0)
            {
                throw new Exception("Generations max count must be a number that is greqter then 0");
            }
            _leftGenerations = _generationsMaxCount - 1;

            var openDlg = new OpenFileDialog
            {
                Title = "Выберите маршрут"
            };
            openDlg.Filter = "Tour file (*.tsp) | *.tsp";
            var result = openDlg.ShowDialog().GetValueOrDefault(false);

            if (result)
            {
                Items = new List<Tuple<double, double>>();
                var lines = File.ReadAllLines(openDlg.FileName).Skip(6).ToArray();
                foreach (var line in lines)
                {
                    if (line == "EOF")
                    {
                        break;
                    }
                    var splitted = line.Split(' ');
                    if (splitted.Length >= 3)
                    {
                        var tuple = new Tuple<double, double>(Double.Parse(splitted[1]), Double.Parse(splitted[2]));
                        Items.Add(tuple);
                    }
                }
                Algo.Items = Items;
                Algo.CreatePopulation(_populationSize);
            }
        }

        public void NextStep()
        {
            if (Items == null || Items.Count == 0)
            {
                throw new Exception("Cities is not loaded");
            }

            if (!HasSolution())
            {
                Algo.NextPopulation();
                _leftGenerations--;
            }
        }

        public bool HasSolution()
        {
            if (_leftGenerations == 0)
            {
                return true;
            }
            var population = Algo.CurrentPopulation[0];
            for (int i = 1; i < Algo.CurrentPopulation.Count; i++)
            {
                if (!population.SequenceEqual(Algo.CurrentPopulation[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public void ShowInfo()
        {
            var bestSolution = Algo.GetBestSolution();
            MinPathSize = String.Format("{0:0.00000}", bestSolution.Item2);
            IList<string> bestRoute = new List<string>();
            foreach (var item in bestSolution.Item1)
            {
                bestRoute.Add((item + 1).ToString());
            }
            BestRoute = bestRoute;
        }

        public void DrawChart()
        {
            ChartModel.Series.Clear();

            var bestSolution = Algo.GetBestSolution();
            var solutions = new LineSeries
            {
                MarkerType = MarkerType.Circle,
                MarkerFill = OxyColor.FromRgb(0, 0, 0xFF),
                MarkerSize = 5
            };
            foreach (var p in bestSolution.Item1)
            {
                var plotPoint = new DataPoint(Convert.ToDouble(Items[p].Item1), Convert.ToDouble(Items[p].Item2));
                solutions.Points.Add(plotPoint);
            }
            ChartModel.Series.Add(solutions);

            ChartModel.InvalidatePlot(true);
        }

        public void Clear()
        {
            ChartModel.Series.Clear();
            ChartModel.InvalidatePlot(true);
            _leftGenerations = 0;
            BestRoute = new List<string>();
            MinPathSize = "";
        }

        public PlotModel ChartModel { get; private set; }

        public GenAlgo Algo { get; private set; }

        public IList<Tuple<double, double>> Items { get; private set; }

        private string _populationSize;
        public String PopulationSize
        {
            get => _populationSize;
            set
            {
                _populationSize = value;
                NotifyPropertyChanged();
            }
        }

        private string _generationsMaxCount;
        public String GenerationMaxCount
        {
            get => _generationsMaxCount;
            set
            {
                _generationsMaxCount = value;
                NotifyPropertyChanged();
            }
        }

        public string _minPathSize;
        public String MinPathSize
        {
            get => _minPathSize;
            set
            {
                _minPathSize = value;
                NotifyPropertyChanged();
            }
        }

        IList<string> _bestRoute;
        public IList<string> BestRoute
        {
            get => _bestRoute;
            set
            {
                _bestRoute = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private int _leftGenerations;

        protected virtual void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
